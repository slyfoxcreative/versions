# Versions

Show version information for Composer and Yarn packages.

## Installation

The package can be installed via Composer to provide the `versions` command in a
PHP project, or the standalone executable can be installed by copying it into a
directory in your `$PATH`. The standalone executable can be updated with the
`self-update` command.
