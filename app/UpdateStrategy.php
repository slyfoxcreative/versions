<?php

declare(strict_types=1);

namespace App;

use Humbug\SelfUpdate\Exception\HttpRequestException;
use Humbug\SelfUpdate\Exception\JsonParsingException;
use Humbug\SelfUpdate\Updater;
use Humbug\SelfUpdate\VersionParser;
use Illuminate\Support\Facades\Http;
use LaravelZero\Framework\Components\Updater\Strategy\StrategyInterface;

class UpdateStrategy implements StrategyInterface
{
    private string $localVersion;
    private string $packageName;
    private array $package;

    public function download(Updater $updater)
    {
        $baseUrl = preg_replace('/\.git$/', '', $this->package['source']['url']);
        $reference = $this->package['source']['reference'];
        $version = $this->package['version'];
        $name = basename(\Phar::running());
        $url = "{$baseUrl}/raw/{$reference}/builds/{$name}";
        $response = Http::get($url);

        if ($response->failed()) {
            throw new HttpRequestException("Request failed: {$url}");
        }

        file_put_contents($updater->getTempPharFile(), $response->body());
    }

    public function getCurrentRemoteVersion(Updater $updater)
    {
        $url = "https://composer.slyfoxcreative.com/p2/{$this->packageName}.json";
        $response = Http::get($url);

        if ($response->failed()) {
            throw new HttpRequestException("Request failed: {$url}");
        }

        try {
            $package = json_decode($response->body(), true, 512, JSON_THROW_ON_ERROR);
        } catch (\JsonException $e) {
            throw new JsonParsingException('Error parsing package data: ' . $e->getMessage());
        }

        $versions = collect($package['packages'][$this->packageName])->map->version->toArray();
        $versionParser = new VersionParser($versions);
        $version = $versionParser->getMostRecentStable();

        if (!is_null($version)) {
            $this->package = collect($package['packages'][$this->packageName])
                ->filter(fn ($p) => $p['version'] === $version)
                ->first()
            ;
        }

        return $version;
    }

    public function setCurrentLocalVersion($version)
    {
        $this->localVersion = $version;
    }

    public function getCurrentLocalVersion(Updater $updater)
    {
        return $this->localVersion;
    }

    public function setPackageName($name)
    {
        $this->packageName = $name;
    }
}
