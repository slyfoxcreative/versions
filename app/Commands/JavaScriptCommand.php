<?php

declare(strict_types=1);

namespace App\Commands;

use LaravelZero\Framework\Commands\Command;
use SlyFoxCreative\PackageManagers\Commands\JavaScriptHandler;

class JavaScriptCommand extends Command
{
    use JavaScriptHandler;

    protected $signature = 'javascript {--file=package.json} {--lockfile=yarn.lock}';

    protected $description = 'Show version information for JavaScript packages';
}
