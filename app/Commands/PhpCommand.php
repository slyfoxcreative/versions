<?php

declare(strict_types=1);

namespace App\Commands;

use LaravelZero\Framework\Commands\Command;
use SlyFoxCreative\PackageManagers\Commands\PhpHandler;

class PhpCommand extends Command
{
    use PhpHandler;

    protected $signature = 'php {--file=composer.json} {--lockfile=composer.lock}';

    protected $description = 'Show version information for PHP packages';
}
