<?php

$finder = PhpCsFixer\Finder::create()
    ->exclude([
        'bootstrap',
        'builds',
    ])
    ->in(__DIR__)
;

return SlyFoxCreative\CodingStyles\config($finder);
